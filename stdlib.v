# Vlinder standard library ^_^

defun: #not [x] [if: x [false] [true]]
    
defun: #when [cond block] [if: cond block [null]]

defun: #do-times [n block] [
    defvar: #count 0
    loop: [
        eval: block
        set!: #count (plus: count 1)
        when: (greater-than-or-equal: count n) [break:]
    ]
]

defun: #not-equal [x y] [not: equal: x y]

defun: #map [f list] [
    defvar: #result []
    for-each: #x list [
        append!: result (f: x)
    ]
    result
]

#
# simple type checking functions

defun: #string? [x] [equal: (type: x) #string]
defun: #integer? [x] [equal: (type: x) #integer]
defun: #symbol? [x] [equal: (type: x) #symbol]
defun: #symbol-literal? [x] [equal: (type: x) #symbol-literal]
defun: #list? [x] [equal: (type: x) #list]
defun: #dictionary? [x] [equal: (type: x) #dictionary]
defun: #boolean? [x] [equal: (type: x) #boolean]

# XXX ^ these *could* be generated programmatically, but right now we lack all
# the parts for this...

# none of the following works, because we're cluttering namespaces. :(
'''
# both set! and the lookup are a problem here. set! uses lexical scoping.
defun: #inc! [name] [
    defvar: #cns caller-namespace:
    debug-info: cns
    set!: name (plus: (eval-object-in: name cns) 1)]
defun: #dec! [name] [set!: name (plus: (eval-object: name) -1)]
# TODO: check possible scoping issues?

defun: #inc! [name] [
    eval-in: [set!: name (plus: (eval-object: name) 1)] caller-namespace:
]
'''

