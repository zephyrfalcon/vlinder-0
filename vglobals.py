# vglobals.py
#
# Global functions. All of these will be wrapped into a VBuiltinFunction, and
# must have the signature (ip, env, values), where ip is a VlinderInterpreter
# instance, env is the environment they are called in (?), and values is a
# list of values passed to the actual function. (All of these must be VTypes;
# list may be empty for some functions.)

import tools
import vtypes

def vfunction(name, arity):
    def vf_decorator(f):
        f.name = name
        f.arity = arity
        return f
    return vf_decorator

@vfunction("print", 1)
def v_print(ip, env, values):
    [x] = values
    print(x.v_str())
    return x

@vfunction("plus", 2)
def v_plus(ip, env, values):
    x, y = values
    # assume integers for now... sigh
    return vtypes.VInteger(x.data + y.data)

@vfunction("defvar", 2)
def v_defvar(ip, env, values):
    varname, value = values
    assert isinstance(varname, vtypes.VSymbol)
    name = vtypes.VSymbol(varname.data)
    env[name] = value
    return value

@vfunction("set!", 2)
def v_set(ip, env, values):
    varname, value = values
    assert isinstance(varname, vtypes.VSymbol)
    name = vtypes.VSymbol(varname.data)
    old_value, var_env = tools.lookup(env, name)
    var_env[name] = value
    return value

# ^ In theory, 'defvar' could be written in Vlinder itself, once we have
# functions to manipulate dictionaries (namespaces)...

@vfunction("eval", 1)
def v_eval(ip, env, values):
    """ Evaluate a code block in the environment associated with it. """
    [code] = values
    new_ns = tools.make_namespace(code.env)
    return ip.eval_and_return_last(code.data, new_ns)

@vfunction("eval-here", 1)
def v_eval_here(ip, env, values):
    [code] = values
    new_ns = tools.make_namespace(env)
    return ip.eval_and_return_last(code.data, new_ns)

@vfunction("eval-in", 2)
def v_eval_in(ip, env, values):
    code, eval_env = values
    return ip.eval_and_return_last(code.data, eval_env)

@vfunction("eval-object", 1)
def v_eval_object(ip, env, values):
    """ Evaluates a Vlinder object. Most everything evaluates to itself,
        including lists (use 'eval' & friends to evaluate code blocks).
        Symbols, however, are looked up. E.g.
        > eval-object: #x
        => (value of x)
    """
    [x] = values
    return ip.eval_object(x, env)

@vfunction("eval-object-in", 2)
def v_eval_object_in(ip, env, values):
    x, eval_env = values
    return ip.eval_object(x, eval_env)

@vfunction("this-namespace", 0)
def v_this_namespace(ip, env, values):
    return env

@vfunction("caller-namespace", 0)
def v_caller_namespace(ip, env, values):
    return env[vtypes.VSymbol("__parent__")]

'''
@vfunction("literal", 1)
def v_literal(ip, env, values):
    [symbol] = values
    assert isinstance(symbol, vtypes.VSymbol)
    return vtypes.VSymbolLiteral(symbol.data)
'''

# TODO: 
# - eval-here: code     => evaluate in current environment
# - eval-in: code env   => evaluate in given env
# - eval-collect: code  => collect the results of all the expressions and
#                          return them as a list
# ...OR, use a suffix to indicate eval-collect behavior, for eval, eval-here
# and eval-in (and whatever else we can come up with)

@vfunction("if", 3)
def v_if(ip, env, values):
    cond, true_block, false_block = values
    assert isinstance(cond, vtypes.VBoolean)
    block = true_block if cond is ip.v_true else false_block
    return ip.eval_and_return_last(block.data, block.env)
    # Q: should it be block.env? normally you create the blocks right there,
    # so it should be no different from "the current environment". but Vlinder
    # does let you use other blocks/lists. not sure what should happen in that
    # case.

@vfunction("defun", 3)
def v_defun(ip, env, values):
    """ defun: name args body 
        Define a custom function. The name must be specified by a symbol
        (literal). The arguments are a list of 0+ elements which will be
        interpreted as symbols. 
    """
    name, args, body = values
    assert isinstance(name, vtypes.VSymbol)
    assert isinstance(args, vtypes.VList)
    assert isinstance(body, vtypes.VList)
    arg_names = []
    for x in args.data:
        assert isinstance(x, vtypes.VSymbol)
        arg_names.append(x.data)
    f = vtypes.VUserDefinedFunction(name.data, arg_names, body)
    env[name] = f
    return f

@vfunction("lambda", 2)
def v_lambda(ip, env, values):
    """ lambda: args body """
    args, body = values
    assert isinstance(args, vtypes.VList)
    assert isinstance(body, vtypes.VList)
    arg_names = []
    for x in args.data:
        assert isinstance(x, vtypes.VSymbol)
        arg_names.append(x.data)
    f = vtypes.VUserDefinedFunction(None, arg_names, body)
    return f

class BreakError(Exception): pass
class ContinueError(Exception): pass

@vfunction("loop", 1)
def v_loop(ip, env, values):
    [block] = values
    result = ip.v_null
    while True:
        try:
            result = ip.eval_and_return_last(block.data, env) # or block.env?
        except BreakError:
            return result
        except ContinueError:
            pass

@vfunction("break", 0)
def v_break(ip, env, values):
    raise BreakError

@vfunction("continue", 0)
def v_continue(ip, env, values):
    raise ContinueError

@vfunction("less-than", 2)
def v_less_than(ip, env, values):
    this, that = values
    return ip.v_true if this < that else ip.v_false

@vfunction("greater-than", 2)
def v_greater_than(ip, env, values):
    this, that = values
    return ip.v_true if this > that else ip.v_false

@vfunction("greater-than-or-equal", 2)
def v_greater_than_or_equal(ip, env, values):
    this, that = values
    return ip.v_true if this >= that else ip.v_false

@vfunction("less-than-or-equal", 2)
def v_less_than_or_equal(ip, env, values):
    this, that = values
    return ip.v_true if this <= that else ip.v_false

@vfunction("equal", 2)
def v_equal(ip, env, values):
    this, that = values
    return ip.v_true if this == that else ip.v_false

@vfunction("for-each", 3)
def v_for_each(ip, env, values):
    """ for-each: name list block """
    # NOTE: in theory we could implement this with loop and list accessors...
    # we would then also be able to use break/continue inside the block.
    name, values, block = values
    for x in values.data:
        new_env = vtypes.VDictionary()
        new_env[vtypes.VSymbol("__parent__")] = block.env
        new_env[name] = x
        ip.eval_all_code(block.data, new_env)
    return ip.v_null

@vfunction("append!", 2)
def v_append_excl(ip, env, values):
    """ append!: list x
        Append x to list, changing it in-place. """
    lst, x = values
    assert isinstance(lst, vtypes.VList)
    lst.data.append(x)
    return lst

@vfunction("debug-info", 1)
def v_debug_info(ip, env, values):
    [x] = values
    if isinstance(x, vtypes.VDictionary):
        print("** DEBUG: VDictionary")
        for key, value in x.data.items():
            print("%s => %s" % (key, value))
    else:
        print("** DEBUG: %s" % x)
    return ip.v_null

@vfunction("type", 1)
def v_type(ip, env, values):
    """ Simple type function that returns the name of the VType. """
    [x] = values
    return vtypes.VSymbol(x.__class__.name)

