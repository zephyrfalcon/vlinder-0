# tools.py

import vtypes

def lookup(env, symbol):
    assert isinstance(symbol, vtypes.VSymbol)
    assert isinstance(env, vtypes.VDictionary)
    try:
        return env[symbol], env
    except KeyError:
        # look at __parent__ attribute
        parent = env.data.get(vtypes.VSymbol("__parent__"))
        if parent is not None:
            return lookup(parent, symbol)
        else:
            raise

def make_namespace(parent):
    ns = vtypes.VDictionary()
    ns[vtypes.VSymbol("__parent__")] = parent
    ns[vtypes.VSymbol("__type__")] = vtypes.VSymbol("namespace")
    return ns

