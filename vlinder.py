#!/usr/bin/env python3
# vlinder.py
# Prototype for the Vlinder programming language.

import getopt
import os
import re
import sys
import types
from vtypes import *
import tools

__version__ = "0.0.36"

identifier_pattern = "[a-zA-Z0-9_?!@+*/-]+:?" 
# add characters as desired. note: has to end with '-'. 

# We have tokens for literals, brackets and symbols. Within symbols, there may
# be additional differences (e.g. a function call header ends with ":", but we
# don't distinguish between those at the tokenizer level).

token_res = [
    ('string', re.compile(r'\".*\"')),
    ('float', re.compile(r"-?\d+\.\d+")),
    ('integer', re.compile(r"-?\d+")),
    ('left_bracket', re.compile(r"\[")),
    ('right_bracket', re.compile(r"\]")),
    ('symbol_literal', re.compile("#" + identifier_pattern)),
    ('symbol', re.compile(identifier_pattern)),
]

def determine_token_type(s):
    for tt, regex in token_res:
        if regex.match(s) is not None:
            return (tt, s)
    raise ValueError("Unknown token: %s" % s)

BOUNDARY_CHARS = '"[] \n\t\r()'

def tokenize(s):
    tokens = []
    while s:
        s = s.lstrip()
        if not s: 
            break
        if s.startswith("["):
            tokens.append(('left_bracket', '['))
            s = s[1:]
        elif s.startswith("]"):
            tokens.append(('right_bracket', ']'))
            s = s[1:]
        elif s.startswith("# ") or s.startswith("#\n"):
            # comments are ignored
            idx = s.find('\n')
            if idx == -1:
                # if no \n can be found, the comment is assumed to be until
                # end of string
                s = ""
            else:
                s = s[idx+1:]
        elif s.startswith('"'):
            strlit = '"'
            s = s[1:]
            while s:
                if s.startswith(r'\"'):
                    strlit += s[:2]
                    s = s[2:]
                elif s.startswith('"'):
                    strlit += '"'
                    s = s[1:]
                    break
                else:
                    strlit += s[0]
                    s = s[1:]
            tokens.append(('string', strlit))
        elif s.startswith("(") or s.startswith(")"):
            s = s[1:]
        elif s.startswith("'''"):
            idx = s.find("'''", 3)
            if idx > -1:
                tokens.append(('string', s[3:idx]))
                s = s[idx+3:]
            else:
                raise SyntaxError("Undelimited multiline string")
        else:
            word = ""
            while s and s[0] not in BOUNDARY_CHARS:
                word = word + s[0]
                s = s[1:]
            t = determine_token_type(word)
            tokens.append(t)
    return tokens

def is_funcall(s):
    """ Return True if a string is a valid function hall header. """
    return s.endswith(":")

class VlinderInterpreter:

    def __init__(self):
        # create an environment for built-in functions
        self.builtins_env = self.make_builtins_env()
        # and a global namespace that derives from it
        self.global_env = tools.make_namespace(self.builtins_env)
        self.debug_options = {
            'show_tokens': False,
        }

    def group_tokens(self, tokens):
        """ Group a list of tokens, i.e. collect tokens between '[' and ']'
            tokens into lists (with the pseudo-token 'list').
            Lists are recursively parsed. At this point, no values are evaluated.
        """
        # NOTE: We DON'T replace with VString etc here; just take a stream of
        # tokens and add structure to it, i.e. create nested lists,
        # represented as Python lists at this point (NOT VLists).
        # That is all.
        data = [[]] # a stack
        while tokens:
            # NOTE: represent lists as ('list', [...])
            token = token_type, value = tokens.pop(0)
            if token_type == "left_bracket":
                data.append([])
            elif token_type == "right_bracket":
                top = data.pop()
                data[-1].append(('list', top))
            else:
                data[-1].append(token)
        if len(data) != 1:
            raise ValueError("Unbalanced lists")
        return data[0]

    def parse_token(self, token):
        """ Convert a token (type, value) to a Vlinder object. Do not look up
            or evaluate anything. """
        token_type, value = token
        if token_type == "list":
            vvalues = [self.parse_token(x) for x in value]
            return VList(vvalues, None)
            # NOTE: values in a VList are NOT evaluated. they will be either
            # atoms (numbers, strings, etc) or unevaluated symbols. nor is any
            # kind of structure imposed on them. (e.g. they will not be
            # considered regular Vlinder code unless we explicitly parse them
            # that way.)
            # the env is just a placeholder for now.
        elif token_type == "string":
            return VString(value[1:-1])
        elif token_type == "integer":
            return VInteger(int(value))
        elif token_type == "symbol_literal":
            return VSymbolLiteral(value[1:])
        elif token_type == "symbol":
            return VSymbol(value)
        # TODO: more types...
        else:
            raise ValueError("Unknown token: %s" % token)

    def parse_tokens(self, data):
        """ Evaluate the next expression found in data (a list of tokens). Return 
            the result and the remaining items in data."""
        return [self.parse_token(x) for x in data]

    def eval_code(self, data, env):
        """ Evaluate a list of Vlinder objects. Stops at the first evaluated
            expression and returns a tuple consisting of the result and the
            remaining (unevaluated) values. """
        head, data = data[0], data[1:]
        x = self.eval_object(head, env)
        if isinstance(head, VSymbol) and is_funcall(head.data) and \
        isinstance(x, VFunction):
            values = []
            for i in range(x.arity):
                value, data = self.eval_code(data, env)
                values.append(value)
            result = x.call(self, env, values)
            return result, data
        else:
            return x, data

    def eval_object(self, x, env):
        """ Like parse_token(), but uses a Vlinder object rather than a token. """
        if isinstance(x, VSymbol):
            if is_funcall(x.data):
                # a symbol ending in ":" is a function call head; look up the
                # symbol minus the colon
                f, _ = tools.lookup(env, VSymbol(x.data[:-1]))
                return f
            else:
                # just look up the symbol
                v, _ = tools.lookup(env, x)
                return v
        elif isinstance(x, VSymbolLiteral):
            return VSymbol(x.data)
        elif isinstance(x, VList):
            # the first time we evaluate a VList, it counts as being "created"
            # and so we set the environment. do not do this repeatedly.
            if x.env is None: 
                x.env = env
            return x
        else:
            # anything else evaluates to itself
            return x

    def eval_all_code(self, data, env):
        results = []
        while data:
            result, data = self.eval_code(data, env)
            results.append(result)
        return results

    def eval_and_return_last(self, data, env):
        results = self.eval_all_code(data, env)
        return results[-1] if results else self.v_null

    def eval_all(self, text, env=None):
        """ Evaluate the given string and return all values it produces. """
        env = env or self.global_env
        tokens = tokenize(text)
        if self.debug_options['show_tokens']:
            print("#DEBUG tokens: ", tokens)
        parts = self.group_tokens(tokens) # still tokens, but now grouped
        vobjs = self.parse_tokens(parts)
        results = []
        while vobjs:
            result, vobjs = self.eval_code(vobjs, env) 
            results.append(result)
        return results

    def make_builtins_env(self):
        import vglobals

        env = VDictionary()
        env.data[VSymbol("__type__")] = VSymbol("namespace")

        self.v_true = env[VSymbol("true")] = VBoolean(True)
        self.v_false = env[VSymbol("false")] = VBoolean(False)
        self.v_null = env[VSymbol("null")] = VNull()

        for key, value in vglobals.__dict__.items():
            if key.startswith("v_") and type(value) is types.FunctionType \
            and hasattr(value, 'arity'):
                vf = VBuiltinFunction(value, value.name, value.arity)
                env.data[VSymbol(value.name)] = vf
        return env

    def load_stdlib(self):
        whereami = os.path.dirname(os.path.abspath(__file__))
        stdlib_name = os.path.join(whereami, "stdlib.v")
        with open(stdlib_name) as f:
            src = f.read()
        self.eval_all(src, env=self.builtins_env)

    def mainloop(self):
        self.load_stdlib()
        while True:
            s = input("> ")
            results = self.eval_all(s)
            for result in results:
                print(result.v_repr())

if __name__ == "__main__":

    vip = VlinderInterpreter()

    opts, args = getopt.getopt(sys.argv[1:], "d:", ["debug="])
    for o, a in opts:
        if o in ("-d", "--debug"):
            dopts = [s.lower().strip() for s in a.split(":")]
            for dopt in dopts:
                if dopt in vip.debug_options:
                    vip.debug_options[dopt] = True
                else:
                    print("Warning: unknown debugging option: %s" % dopt)
    
    vip.mainloop()


