### Goals for this implementation

Vlinder is supposed to be a highly dynamic programming language, with
some experimental features. 

For now, I just want a simple prototype that works. The basics, anyway.
This means I won't care about such trivialities as:

* performance
* tail recursion / tail call optimization
* decent type system
* object system(s)
* multimethods
* concurrency / threads / parallelism
* generators
* literals for hexadecimal, octal, regexen, etc
* continuations

...and probably lots more things. All those can wait, until (and if) the
prototype has proven to be viable. When in doubt, a feature or
potential issue goes on this list.

What I do care about:

* custom parsing of lists
* modules
* namespaces (and possibly special syntax to access them)

I am especially interested in seeing what I can do with the custom
parsing of lists, not unlike Rebol/Red.

