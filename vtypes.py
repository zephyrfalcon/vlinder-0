# vtypes.py

# NOTE: Make sure to define __hash__ and __eq__ so we can use these as keys in
# dictionaries (assuming they're immutable).

class VType:
    def is_mutable(self): return False
    def __repr__(self):
        # assumes we have self.data; override if necessary
        return "%s(%r)" % (self.__class__.__name__, self.data)
    def v_repr(self):
        raise NotImplementedError
    def v_str(self):
        return self.v_repr()

class VString(VType):
    name = "string"
    def __init__(self, s):
        self.data = s
    def __hash__(self):
        return hash("VString::" + self.data)
    def __eq__(self, other):
        return self.data == other.data
    def v_repr(self):
        # FIXME -- we should not piggyback Python's string (un)quoting; we
        # should have our own
        return repr(self.data)
    def v_str(self):
        return self.data

class VInteger(VType):
    name = "integer"
    def __init__(self, i):
        self.data = i
    def __hash__(self):
        return self.data
    def __eq__(self, other):
        return self.data == other.data
    def v_repr(self):
        return str(self.data)
    def __lt__(self, other):
        return self.data < other.data
    def __gt__(self, other):
        return self.data > other.data
    def __le__(self, other):
        return self.data <= other.data
    def __ge__(self, other):
        return self.data >= other.data

# XXX should these be cached so we always have the same symbol?
class VSymbol(VType):
    name = "symbol"
    def __init__(self, s):
        self.data = s
    def __hash__(self):
        return hash("VSymbol::" + self.data)
    def __eq__(self, other):
        return self.data == other.data
    def v_str(self):
        return self.data
    def v_repr(self):
        #return "#%s" % self.data # FIXME
        return self.data # Q: what about leading '#'?

class VSymbolLiteral(VType):
    name = "symbol-literal"
    def __init__(self, s):
        self.data = s
    def __hash__(self):
        return hash("VSymbolLiteral::" + self.data)
    def __eq__(self, other):
        return self.data == other.data
    def v_str(self):
        return self.data
    def v_repr(self):
        return "#%s" % self.data 

class VFunction(VType):
    name = "function"

class VBuiltinFunction(VFunction):
    name = "builtin-function"
    def __init__(self, f, name, arity):
        self.f = f
        self.name = name
        self.arity = arity
    def call(self, ip, env, values):
        assert len(values) == self.arity
        return self.f(ip, env, values)
        # XXX does the passed env come into play anywhere? it should...
        # XXX also should we store the env it was created in? for a builtin??
    def v_repr(self):
        return "<%s>" % self.name
    def __repr__(self):
        return "VBuiltinFunction(%s)" % self.name

class VUserDefinedFunction(VFunction):
    name = "user-defined-function"
    def __init__(self, name, args, body):
        assert isinstance(body, VList)
        self.name = name # plain old string (or None for a lambda?)
        self.args = args # list of plain strings
        self.arity = len(args)
        self.body = body
    def call(self, ip, env, values):
        assert len(values) == self.arity
        # create a new environment
        new_env = VDictionary()
        new_env[VSymbol("__type__")] = VSymbol("namespace")
        # set the body's environment as its parent
        new_env[VSymbol("__parent__")] = self.body.env
        # associate names in self.args with the values given
        for name, value in zip(self.args, values):
            new_env[VSymbol(name)] = value
        # evaluate the body in this new environment
        return ip.eval_and_return_last(self.body.data, new_env)
    def __repr__(self):
        return "VUserDefinedFunction(%s)" % self.name
    def v_repr(self):
        return "#defun<%s>" % (self.name or "(unknown)")
    def __hash__(self):
        return hash(self.__dict__)
    def __eq__(self, other):
        return self.name == other.name \
           and self.args == other.args \
           and self.body == other.body

class VDictionary(VType):
    name = "dictionary"
    def __init__(self):
        self.data = {}
    def __setitem__(self, key, value):
        assert isinstance(key, VType)
        assert isinstance(value, VType)
        assert not key.is_mutable()
        self.data[key] = value
    def __getitem__(self, key):
        return self.data[key]
    def is_mutable(self): return True

class VList(VType):
    name = "list"
    # all VLists are associated with an environment (namespace), whether we
    # use it or not.
    def __init__(self, data, env):
        self.data = data
        self.env = env
    def is_mutable(self): return True
    def __repr__(self):
        return "VList(%s)" % self.data
    def v_repr(self):
        return "[" + " ".join(x.v_repr() for x in self.data) + "]"

class VNull(VType):
    name = "null"
    def __init__(self):
        pass
    def __repr__(self):
        return "VNull"
    def v_repr(self):
        return "null"
    def __hash__(self):
        return hash(None)
    def __eq__(self, other):
        return isinstance(other, self.__class__)

class VBoolean(VType):
    name = "boolean"
    def __init__(self, data):
        self.data = data
    def __repr__(self):
        return "VBoolean(%s)" % self.data
    def v_repr(self):
        return "true" if self.data else "false"
    def __hash__(self):
        return hash(self.data)
    def __eq__(self, other):
        return self.data == other.data
