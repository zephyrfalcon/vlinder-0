# test_vlinder.py

import os
import unittest
import vlinder

# simple function call
CODE1 = 'print: "Hello"'

# function call with one level of nesting
CODE2 = "print: plus: 3 4"

# function call with more nesting
CODE3 = "times: 3 plus: 4 5"

CODE4 = "define: #magic 42" # XXX replace with defvar?

CODE5 = "defun: #square [ x ] [ times: x x ]"

CODE6 = "length: [ 1 2 3 ]"

class TestVlinderTokenizer(unittest.TestCase):
    
    def test_tokenizer_1(self):
        tokens = vlinder.tokenize(CODE1)
        self.assertEquals(tokens, 
          [('symbol', 'print:'), ('string', '"Hello"')])

    def test_tokenizer_2(self):
        tokens = vlinder.tokenize(CODE2)
        self.assertEquals(tokens, 
          [('symbol', 'print:'), ('symbol', 'plus:'), ('integer', '3'), 
           ('integer', '4')])

    def test_tokenizer_3(self):
        tokens = vlinder.tokenize(CODE3)
        self.assertEquals(tokens, 
          [('symbol', 'times:'), ('integer', '3'), ('symbol', 'plus:'), 
           ('integer', '4'), ('integer', '5')])

    def test_tokenizer_4(self):
        tokens = vlinder.tokenize(CODE4)
        self.assertEquals(tokens, 
          [('symbol', 'define:'), ('symbol_literal', '#magic'), 
           ('integer', '42')])

    def test_tokenizer_5(self):
        tokens = vlinder.tokenize(CODE5)
        self.assertEquals(tokens, 
          [('symbol', 'defun:'), ('symbol_literal', '#square'), 
           ('left_bracket', '['), ('symbol', 'x'), ('right_bracket', ']'),
           ('left_bracket', '['), ('symbol', 'times:'), ('symbol', 'x'),
           ('symbol', 'x'), ('right_bracket', ']')])

class TestVlinderParser(unittest.TestCase):

    def setUp(self):
        self.vip = vlinder.VlinderInterpreter()

    def test_parser_1(self):
        tokens = vlinder.tokenize(CODE1)
        stuff = self.vip.group_tokens(tokens)
        self.assertEquals(stuff, [('symbol', 'print:'), ('string', '"Hello"')])

    def test_parser_6(self):
        tokens = vlinder.tokenize(CODE6)
        stuff = self.vip.group_tokens(tokens)
        self.assertEquals(stuff,
          [('symbol', 'length:'), 
           ('list', [('integer', '1'), ('integer', '2'), ('integer', '3')])])

#
# custom Vlinder tests

class TestVlinderCode(unittest.TestCase):
    def setUp(self):
        self.vip = vlinder.VlinderInterpreter()

# find the 'tests' directory
whereami = os.path.dirname(os.path.abspath(__file__))
test_dir = os.path.join(whereami, "tests")
# collect all *.txt files in it
test_files = [os.path.join(test_dir, fn) for fn in os.listdir(test_dir) 
              if fn.endswith(".txt")]

def _make_test_method(code, result):
    def f(self):
        check_code(code, result)
    return f

test_data = []
for fn in test_files:
    shortname, _ = os.path.splitext(os.path.basename(fn))
    with open(fn, 'r') as f:
        current_test_data = []
        for line in f.readlines():
            if line.startswith("=>"):
                test_code = "\n".join(current_test_data).strip()
                t = (shortname, test_code, line[2:].strip())
                test_data.append(t)
                current_test_data = []
            else:
                current_test_data.append(line)

        # this code is used for unittest...
        for idx, (fn, code, result) in enumerate(test_data): 
            f = _make_test_method(code, result)
            method_name = "test_%s_%02d" % (shortname, idx+1)
            setattr(TestVlinderCode, method_name, f)

# this code will run the tests through nose
def test_generator():
    for fn, code, result in test_data:
        yield check_code, fn, code, result

def check_code(fn, code, result):
    vip = vlinder.VlinderInterpreter()
    vip.load_stdlib()
    results = vip.eval_all(code)
    assert str(result) == results[-1].v_repr(), \
      "Expected %r, got %r instead" % (result, results[-1].v_repr())

if __name__ == "__main__":
    unittest.main()

