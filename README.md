This is **Vlinder**, an interpreted, dynamically typed programming language.
Mostly it's yet another vehicle for my programming language ideas and
experiments. This is a crude prototype written in Python; at this point I am
more interested in what I can *do* with the language, than in
performance, or fancy features. (Also see [goals](GOALS.md).)

Inspirations for the language include Logo, Scheme, Python and Rebol.

## Requirements

* Python 3.x. I am using Python 3.5, but it will probably work with any
  older version in the 3.x series.

* [nosetests](http://nose.readthedocs.io/en/latest/) (if you want to run the tests).

